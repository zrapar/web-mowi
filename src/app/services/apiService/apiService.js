import axios from 'axios';
import { domain, contentTypeNoAuth, contentTypeAuth, contentTypeFormAuth } from './apiServiceConfig';

class apiService {
	domain() {
		return process.env.REACT_APP_API_MODE === 'UP' && window.location.host !== 'localhost:3000'
			? domain.up
			: domain.local;
	}

	init() {
		axios.defaults.baseURL = this.domain();

		this.initialData = {
			success      : null,
			access_token : '',
			token_type   : '',
			user         : null
		};

		return this.handleAuthentication();
	}

	login = async (body) => {
		try {
			const res = await axios.post('/auth/login', body, contentTypeNoAuth);
			const { data: { success, ...restData } } = res;

			if (success) {
				this.setSession(restData);
				restData.success = true;
				restData.user.role = [ 'admin' ];
				return restData;
			}
		} catch ({ response, request }) {
			if (response && response.data) {
				return {
					success : false,
					message : response.data.message
				};
			}

			if (request && request.status !== 0) {
				return {
					success : false,
					message : 'Ha ocurrido un error, intente de nuevo'
				};
			}
			return {
				success : false,
				message : 'Revise su conexion a internet'
			};
		}
	};

	checkToken = async (token) => {
		try {
			const res = await axios.get(`/api/auth/password/find/${token}`, contentTypeNoAuth);
			return res.data;
		} catch ({ response, request }) {
			if (response && response.data) {
				return { success: false, message: response.data.message };
			}

			if (request && request.status !== 0) {
				return {
					success : false,
					message : 'Ha ocurrido un error, intente de nuevo'
				};
			}
			return { success: false, message: 'Revise su conexion a internet' };
		}
	};

	recoverPass = async (body) => {
		try {
			const res = await axios.post('/api/auth/password/reset', JSON.stringify(body), contentTypeNoAuth);
			return res.data;
		} catch ({ response, request }) {
			if (response && response.data) {
				return { success: false, message: response.data.message };
			}

			if (request && request.status !== 0) {
				return {
					success : false,
					message : 'Ha ocurrido un error, intente de nuevo'
				};
			}
			return { success: false, message: 'Revise su conexion a internet' };
		}
	};

	activateAccount = async (token) => {
		try {
			const res = await axios.post(`/api/auth/activate/${token}`, contentTypeNoAuth);
			return res.data;
		} catch ({ response, request }) {
			if (response && response.data) {
				return { success: false, message: response.data.message };
			}

			if (request && request.status !== 0) {
				return {
					success : false,
					message : 'Ha ocurrido un error, intente de nuevo'
				};
			}
			return { success: false, message: 'Revise su conexion a internet' };
		}
	};

	register = async (data) => {
		if (!this.initialData) {
			return false;
		}
		// console.log('data', data);

		// const res = await axios.post(
		// 	'/api/users/store',
		// 	data,
		// 	contentTypeNoAuth
		// );
		//
	};

	handleAuthentication = async () => {
		if (!this.initialData) {
			return false;
		}

		return this.isAuthenticated();
	};

	setSession = ({ user, token, token_type, expires_in }) => {
		window.localStorage.setItem('access_token', token);
		window.localStorage.setItem('expires_at', expires_in);

		axios.defaults.headers.common['Authorization'] = `${token_type} ${token}`;
	};

	logout = () => {
		window.localStorage.removeItem('mowi');
		window.localStorage.removeItem('access_token');
		window.localStorage.removeItem('expires_at');
		window.localStorage.clear();
	};

	isAuthenticated = async () => {
		if (!this.initialData) {
			return false;
		}
		// Check whether the current time is past the
		// access token's expiry time
		const dateExpire = window.localStorage.getItem('expires_at');
		const accessToken = window.localStorage.getItem('access_token');

		if (dateExpire && accessToken) {
			// let expiresAt = new Date(dateExpire.replace(/-/g, '/')).getTime();
			// const isNotExpired = new Date().getTime() < expiresAt;
			const { data } = await this.getUserData();
			let user = data;
			user.role = [ 'admin' ];
			delete user.id;
			axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`;

			return { success: true, user };
		}

		this.logout();
		return { success: false, user: null };
	};

	getTokenByRefresh = async (refresh_token) => {
		const bodyToken = {
			refresh_token,
			scope         : '',
			client_id     : process.env.REACT_APP_API_CLIENT_ID,
			client_secret : process.env.REACT_APP_API_CLIENT_SECRECT,
			grant_type    : 'refresh_token'
		};
		try {
			const res2 = await axios.post('/oauth/token', bodyToken, contentTypeNoAuth);

			if (res2.data.hasOwnProperty('access_token')) {
				this.setTokenSessionRefresh(res2.data);
				return true;
			}
		} catch ({ response, request }) {
			if (response && response.data) {
				return {
					success : false,
					message : response.data.message
				};
			}

			if (request && request.status !== 0) {
				return {
					success : false,
					message : 'Ha ocurrido un error, intente de nuevo'
				};
			}
			return {
				success : false,
				message : 'Revise su conexion a internet'
			};
		}
	};

	setTokenSessionRefresh = (data) => {
		const expired = window.localStorage.getItem('expires_at');
		let expiresAt = new Date(expired.replace(/-/g, '/')).getTime();
		const noFormatDate = new Date(expiresAt + data.expires_in * 1000);

		var newExpired =
			noFormatDate.getUTCFullYear() +
			'-' +
			('0' + (noFormatDate.getUTCMonth() + 1)).slice(-2) +
			'-' +
			('0' + noFormatDate.getUTCDate()).slice(-2) +
			' ' +
			('0' + noFormatDate.getUTCHours()).slice(-2) +
			':' +
			('0' + noFormatDate.getUTCMinutes()).slice(-2) +
			':' +
			('0' + noFormatDate.getUTCSeconds()).slice(-2);

		window.localStorage.removeItem('access_token');
		window.localStorage.removeItem('refresh_token');
		window.localStorage.removeItem('expires_at');
		window.localStorage.setItem('access_token', data.access_token);
		window.localStorage.setItem('refresh_token', data.refresh_token);
		window.localStorage.setItem('expires_at', newExpired);
	};

	getUserData = async () => {
		try {
			const res = await axios.get('/auth/me', contentTypeAuth(this.getAccessToken()));
			return res.data;
		} catch ({ response, request }) {
			if (response && response.data) {
				return {
					success : false,
					message : response.data.message
				};
			}

			if (request && request.status !== 0) {
				return {
					success : false,
					message : 'Ha ocurrido un error, intente de nuevo'
				};
			}
			return {
				success : false,
				message : 'Revise su conexion a internet'
			};
		}
	};

	getAccessToken = () => {
		return window.localStorage.getItem('access_token');
	};
}

const instance = new apiService();

export default instance;
