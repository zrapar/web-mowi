export const domain = {
	up    : 'https://api.captainomega.com/api',
	local : 'http://127.0.0.1:8000/api'
};

export const contentTypeNoAuth = {
	headers : {
		'Content-Type' : 'application/json'
	}
};

export const contentTypeAuth = (token) => {
	return {
		headers : {
			'Content-Type' : 'application/json',
			Authorization  : `Bearer ${token}`
		}
	};
};

export const contentTypeFormAuth = (token) => {
	return {
		headers : {
			'Content-Type' : 'multipart/form-data',
			Authorization  : `Bearer ${token}`
		}
	};
};
