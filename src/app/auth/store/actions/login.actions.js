import apiService from 'app/services/apiService';
import { setUserData } from './user.actions';
import * as Actions from 'app/store/actions';

export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';

export const loginApi = (body) => async (dispatch) => {
	const { user, success, message } = await apiService.login(body);
	dispatch(setUserData(user));

	if (success) {
		dispatch(Actions.showMessage({ message: `Bienvenido ${user.name}` }));
		return dispatch({
			type : LOGIN_SUCCESS
		});
	}

	return dispatch({
		type    : LOGIN_ERROR,
		payload : message
	});
};
