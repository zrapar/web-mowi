import axios from 'axios';
import { logoutUser } from 'app/auth/store/actions';
import { showMessage } from 'app/store/actions';

export const GET_MARKETINGS = '@@Marketings / GET MARKETINGS';
export const GET_DATA_TO_USE = '@@Marketings / GET DATA TO USE';
export const SET_SEARCH_TEXT = '@@Marketings / SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_MARKETINGS = '@@Marketings / TOGGLE IN SELECTED MARKETINGS';
export const SELECT_ALL_MARKETINGS = '@@Marketings / SELECT ALL MARKETINGS';
export const DESELECT_ALL_MARKETINGS = '@@Marketings / DESELECT ALL MARKETINGS';
export const OPEN_NEW_MARKETING_DIALOG = '@@Marketings / OPEN NEW MARKETING DIALOG';
export const CLOSE_NEW_MARKETING_DIALOG = '@@Marketings / CLOSE NEW MARKETING DIALOG';
export const OPEN_EDIT_MARKETING_DIALOG = '@@Marketings / OPEN EDIT MARKETING DIALOG';
export const CLOSE_EDIT_MARKETING_DIALOG = '@@Marketings / CLOSE EDIT MARKETING DIALOG';
export const ADD_MARKETING = '@@Marketings / ADD MARKETING';
export const UPDATE_MARKETING = '@@Marketings / UPDATE MARKETING';
export const REMOVE_MARKETING = '@@Marketings / REMOVE MARKETING';
export const REMOVE_MARKETINGS = '@@Marketings / REMOVE MARKETINGS';

export const getMarketings = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/marketing', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_MARKETINGS,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const getDataToUse = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/brands', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_DATA_TO_USE,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const setSearchText = (event) => ({
	type       : SET_SEARCH_TEXT,
	searchText : event.target.value
});

export const toggleInSelectedMarketings = (marketingId) => ({
	type        : TOGGLE_IN_SELECTED_MARKETINGS,
	marketingId
});

export const selectAllMarketings = () => ({
	type : SELECT_ALL_MARKETINGS
});

export const deSelectAllMarketings = () => ({
	type : DESELECT_ALL_MARKETINGS
});

export const openNewMarketingsDialog = () => ({
	type : OPEN_NEW_MARKETING_DIALOG
});

export const closeNewMarketingsDialog = () => ({
	type : CLOSE_NEW_MARKETING_DIALOG
});

export const openEditMarketingsDialog = (data) => ({
	type : OPEN_EDIT_MARKETING_DIALOG,
	data
});

export const closeEditMarketingsDialog = () => ({
	type : CLOSE_EDIT_MARKETING_DIALOG
});

export const addMarketing = (newMarketing) => async (dispatch) => {
	try {
		const res = await axios.post('/marketing', newMarketing, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;
		if (success) {
			Promise.all([
				dispatch({
					type : ADD_MARKETING
				})
			]).then(() => dispatch(getMarketings()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const updateMarketing = (marketing, uuid) => async (dispatch) => {
	try {
		const res = await axios.post(`/marketing/${uuid}`, marketing, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : UPDATE_MARKETING
				})
			]).then(() => dispatch(getMarketings()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeMarketing = (marketingId) => async (dispatch) => {
	try {
		const res = await axios.delete(`/marketing/${marketingId}`);
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : REMOVE_MARKETING
				})
			]).then(() => dispatch(getMarketings()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeMarketings = (marketingIds) => (dispatch) => {
	let arrayPromise = [];
	marketingIds.forEach((uuid) => {
		const req = axios.delete(`/marketing/${uuid}`);
		arrayPromise.push(req);
	});

	arrayPromise.push(
		dispatch({
			type : REMOVE_MARKETINGS
		})
	);

	Promise.all(arrayPromise).then(() => dispatch(getMarketings()));
	dispatch(
		showMessage({
			message : 'Successfully deleted',
			variant : 'success'
		})
	);
};
