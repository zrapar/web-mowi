import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
	entities            : null,
	searchText          : '',
	selectedMarketingIds : [],
	routeParams         : {},
	brands              : [],
	marketingDialog      : {
		type  : 'new',
		props : {
			open : false
		},
		data  : null
	}
};

const marketingsReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_MARKETINGS: {
			return {
				...state,
				entities    : _.keyBy(action.payload, 'uuid'),
				routeParams : action.routeParams
			};
		}
		case Actions.SET_SEARCH_TEXT: {
			return {
				...state,
				searchText : action.searchText
			};
		}
		case Actions.TOGGLE_IN_SELECTED_MARKETINGS: {
			const marketingId = action.marketingId;

			let selectedMarketingIds = [ ...state.selectedMarketingIds ];

			if (selectedMarketingIds.find((id) => id === marketingId) !== undefined) {
				selectedMarketingIds = selectedMarketingIds.filter((id) => id !== marketingId);
			} else {
				selectedMarketingIds = [ ...selectedMarketingIds, marketingId ];
			}

			return {
				...state,
				selectedMarketingIds : selectedMarketingIds
			};
		}
		case Actions.SELECT_ALL_MARKETINGS: {
			const arr = Object.keys(state.entities).map((k) => state.entities[k]);

			const selectedMarketingIds = arr.map((marketing) => marketing.id);

			return {
				...state,
				selectedMarketingIds : selectedMarketingIds
			};
		}
		case Actions.DESELECT_ALL_MARKETINGS: {
			return {
				...state,
				selectedMarketingIds : []
			};
		}
		case Actions.OPEN_NEW_MARKETING_DIALOG: {
			return {
				...state,
				marketingDialog : {
					type  : 'new',
					props : {
						open : true
					},
					data  : null
				}
			};
		}
		case Actions.CLOSE_NEW_MARKETING_DIALOG: {
			return {
				...state,
				marketingDialog : {
					type  : 'new',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.OPEN_EDIT_MARKETING_DIALOG: {
			return {
				...state,
				marketingDialog : {
					type  : 'edit',
					props : {
						open : true
					},
					data  : action.data
				}
			};
		}
		case Actions.CLOSE_EDIT_MARKETING_DIALOG: {
			return {
				...state,
				marketingDialog : {
					type  : 'edit',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.GET_DATA_TO_USE: {
			return {
				...state,
				brands : [ ...action.payload ]
			};
		}
		default: {
			return state;
		}
	}
};

export default marketingsReducer;
