import { combineReducers } from 'redux';
import marketings from './marketings.reducer';

const reducer = combineReducers({
	marketings
});

export default reducer;
