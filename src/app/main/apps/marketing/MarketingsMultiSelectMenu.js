import React, { useState } from 'react';
import { Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList } from '@material-ui/core';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';

const MarketingsMultiSelectMenu = (props) => {
	const dispatch = useDispatch();
	const selectedMarketingIds = useSelector(({ marketings: { marketings } }) => marketings.selectedMarketingIds);

	const [ anchorEl, setAnchorEl ] = useState(null);

	function openSelectedMarketingMenu(event) {
		setAnchorEl(event.currentTarget);
	}

	function closeSelectedMarketingsMenu() {
		setAnchorEl(null);
	}

	return (
		<React.Fragment>
			<IconButton
				className='p-0'
				aria-owns={anchorEl ? 'selectedMarketingsMenu' : null}
				aria-haspopup='true'
				onClick={openSelectedMarketingMenu}
			>
				<Icon>more_horiz</Icon>
			</IconButton>
			<Menu
				id='selectedMarketingsMenu'
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={closeSelectedMarketingsMenu}
			>
				<MenuList>
					<MenuItem
						onClick={() => {
							dispatch(Actions.removeMarketings(selectedMarketingIds));
							closeSelectedMarketingsMenu();
						}}
					>
						<ListItemIcon className='min-w-40'>
							<Icon>delete</Icon>
						</ListItemIcon>
						<ListItemText primary='Remove' />
					</MenuItem>
				</MenuList>
			</Menu>
		</React.Fragment>
	);
};

export default MarketingsMultiSelectMenu;
