import React, { useEffect, useState } from 'react';
import { Checkbox, Icon, IconButton, Typography } from '@material-ui/core';
import { FuseUtils, FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from 'react-table';
import * as Actions from './store/actions';
import MarketingsMultiSelectMenu from './MarketingsMultiSelectMenu';

const MarketingsList = (props) => {
	const dispatch = useDispatch();
	const marketings = useSelector(({ marketings: { marketings } }) => marketings.entities);
	const selectedMarketingIds = useSelector(({ marketings: { marketings } }) => marketings.selectedMarketingIds);
	const searchText = useSelector(({ marketings: { marketings } }) => marketings.searchText);

	const [ filteredData, setFilteredData ] = useState(null);

	useEffect(
		() => {
			function getFilteredArray(entities, searchText) {
				const arr = Object.keys(entities).map((id) => entities[id]);
				if (searchText.length === 0) {
					return arr;
				}
				return FuseUtils.filterArrayByString(arr, searchText);
			}

			if (marketings) {
				setFilteredData(getFilteredArray(marketings, searchText));
			}
		},
		[ marketings, searchText ]
	);

	if (!filteredData) {
		return null;
	}

	if (filteredData.length === 0) {
		return (
			<div className='flex flex-1 items-center justify-center h-full'>
				<Typography color='textSecondary' variant='h5'>
					There are no marketings!
				</Typography>
			</div>
		);
	}

	return (
		<FuseAnimate animation='transition.slideUpIn' delay={300}>
			<ReactTable
				className='-striped -highlight h-full sm:rounded-16 overflow-hidden'
				getTrProps={(state, rowInfo, column) => {
					return {
						className : 'cursor-pointer',
						onClick   : (e, handleOriginal) => {
							if (rowInfo) {
								dispatch(Actions.openEditMarketingsDialog(rowInfo.original));
							}
						}
					};
				}}
				data={filteredData}
				columns={[
					{
						Header    : () => (
							<Checkbox
								onClick={(event) => {
									event.stopPropagation();
								}}
								onChange={(event) => {
									event.target.checked
										? dispatch(Actions.selectAllMarketings())
										: dispatch(Actions.deSelectAllMarketings());
								}}
								checked={
									selectedMarketingIds.length === Object.keys(marketings).length &&
									selectedMarketingIds.length > 0
								}
								indeterminate={
									selectedMarketingIds.length !== Object.keys(marketings).length &&
									selectedMarketingIds.length > 0
								}
							/>
						),
						accessor  : '',
						Cell      : (row) => {
							return (
								<Checkbox
									onClick={(event) => {
										event.stopPropagation();
									}}
									checked={selectedMarketingIds.includes(row.value.id)}
									onChange={() => dispatch(Actions.toggleInSelectedMarketings(row.value.id))}
								/>
							);
						},
						className : 'justify-center',
						sortable  : false,
						width     : 64
					},
					{
						Header     : 'Name',
						accessor   : 'name',
						filterable : true,
						className  : 'font-bold'
					},
					{
						Header     : 'Brand Associate',
						accessor   : 'brands',
						Cell       : (row) => (
							<div className='flex items-center'>
								<span>{row.value.name}</span>
							</div>
						),
						filterable : true,
						className  : 'font-bold'
					},
					{
						Header     : 'Type',
						accessor   : 'type',
						Cell       : (row) => (
							<span>{row.value === 'foods' ? 'Food Service' : 'Consumer Package Goods'}</span>
						),
						filterable : true
					},
					{
						Header : () => selectedMarketingIds.length > 0 && <MarketingsMultiSelectMenu />,
						width  : 128,
						Cell   : (row) => (
							<div className='flex items-center'>
								<IconButton
									onClick={(ev) => {
										ev.stopPropagation();
										dispatch(Actions.removeMarketing(row.original.uuid));
									}}
								>
									<Icon>delete</Icon>
								</IconButton>
							</div>
						)
					}
				]}
				defaultPageSize={10}
				noDataText='No marketings found'
			/>
		</FuseAnimate>
	);
};

export default MarketingsList;
