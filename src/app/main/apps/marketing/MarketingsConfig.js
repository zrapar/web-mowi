import React from 'react';
import { Redirect } from 'react-router-dom';

export const MarketingsConfig = {
	settings : {
		layout : {
			config : {}
		}
	},
	routes   : [
		{
			path      : '/apps/marketing/:id',
			component : React.lazy(() => import('./Marketings'))
		},
		{
			path      : '/apps/marketing',
			component : () => <Redirect to='/apps/marketing/all' />
		}
	]
};
