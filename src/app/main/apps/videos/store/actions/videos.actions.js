import axios from 'axios';
import { logoutUser } from 'app/auth/store/actions';
import { showMessage } from 'app/store/actions';

export const GET_VIDEOS = '@@Videos / GET VIDEOS';
export const GET_DATA_TO_USE = '@@Videos / GET DATA TO USE';
export const SET_SEARCH_TEXT = '@@Videos / SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_VIDEOS = '@@Videos / TOGGLE IN SELECTED VIDEOS';
export const SELECT_ALL_VIDEOS = '@@Videos / SELECT ALL VIDEOS';
export const DESELECT_ALL_VIDEOS = '@@Videos / DESELECT ALL VIDEOS';
export const OPEN_NEW_VIDEO_DIALOG = '@@Videos / OPEN NEW VIDEO DIALOG';
export const CLOSE_NEW_VIDEO_DIALOG = '@@Videos / CLOSE NEW VIDEO DIALOG';
export const OPEN_EDIT_VIDEO_DIALOG = '@@Videos / OPEN EDIT VIDEO DIALOG';
export const CLOSE_EDIT_VIDEO_DIALOG = '@@Videos / CLOSE EDIT VIDEO DIALOG';
export const ADD_VIDEO = '@@Videos / ADD VIDEO';
export const UPDATE_VIDEO = '@@Videos / UPDATE VIDEO';
export const REMOVE_VIDEO = '@@Videos / REMOVE VIDEO';
export const REMOVE_VIDEOS = '@@Videos / REMOVE VIDEOS';

export const getVideos = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/videos', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_VIDEOS,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const getDataToUse = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/brands', {
			params : routeParams
		});

		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_DATA_TO_USE,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const setSearchText = (event) => ({
	type       : SET_SEARCH_TEXT,
	searchText : event.target.value
});

export const toggleInSelectedVideos = (videoId) => ({
	type    : TOGGLE_IN_SELECTED_VIDEOS,
	videoId
});

export const selectAllVideos = () => ({
	type : SELECT_ALL_VIDEOS
});

export const deSelectAllVideos = () => ({
	type : DESELECT_ALL_VIDEOS
});

export const openNewVideosDialog = () => ({
	type : OPEN_NEW_VIDEO_DIALOG
});

export const closeNewVideosDialog = () => ({
	type : CLOSE_NEW_VIDEO_DIALOG
});

export const openEditVideosDialog = (data) => ({
	type : OPEN_EDIT_VIDEO_DIALOG,
	data
});

export const closeEditVideosDialog = () => ({
	type : CLOSE_EDIT_VIDEO_DIALOG
});

export const addVideo = (newVideo) => async (dispatch) => {
	try {
		const res = await axios.post('/videos', newVideo, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});

		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : ADD_VIDEO
				})
			]).then(() => dispatch(getVideos()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const updateVideo = (video, uuid) => async (dispatch) => {
	try {
		const res = await axios.post(`/videos/${uuid}`, video, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});

		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : UPDATE_VIDEO
				})
			]).then(() => dispatch(getVideos()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeVideo = (videoId) => async (dispatch) => {
	try {
		const res = await axios.delete(`/videos/${videoId}`);

		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : REMOVE_VIDEO
				})
			]).then(() => dispatch(getVideos()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeVideos = (videoIds) => (dispatch) => {
	let arrayPromise = [];
	videoIds.forEach((uuid) => {
		const req = axios.delete(`/videos/${uuid}`);
		arrayPromise.push(req);
	});

	arrayPromise.push(
		dispatch({
			type : REMOVE_VIDEOS
		})
	);

	Promise.all(arrayPromise).then(() => dispatch(getVideos()));
	dispatch(
		showMessage({
			message : 'Successfully deleted',
			variant : 'success'
		})
	);
};
