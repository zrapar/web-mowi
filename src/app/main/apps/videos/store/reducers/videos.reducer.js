import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
	entities            : null,
	searchText          : '',
	selectedVideoIds : [],
	routeParams         : {},
	brands              : [],
	videoDialog      : {
		type  : 'new',
		props : {
			open : false
		},
		data  : null
	}
};

const videosReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_VIDEOS: {
			return {
				...state,
				entities    : _.keyBy(action.payload, 'uuid'),
				routeParams : action.routeParams
			};
		}
		case Actions.SET_SEARCH_TEXT: {
			return {
				...state,
				searchText : action.searchText
			};
		}
		case Actions.TOGGLE_IN_SELECTED_VIDEOS: {
			const videoId = action.videoId;

			let selectedVideoIds = [ ...state.selectedVideoIds ];

			if (selectedVideoIds.find((id) => id === videoId) !== undefined) {
				selectedVideoIds = selectedVideoIds.filter((id) => id !== videoId);
			} else {
				selectedVideoIds = [ ...selectedVideoIds, videoId ];
			}

			return {
				...state,
				selectedVideoIds : selectedVideoIds
			};
		}
		case Actions.SELECT_ALL_VIDEOS: {
			const arr = Object.keys(state.entities).map((k) => state.entities[k]);

			const selectedVideoIds = arr.map((video) => video.id);

			return {
				...state,
				selectedVideoIds : selectedVideoIds
			};
		}
		case Actions.DESELECT_ALL_VIDEOS: {
			return {
				...state,
				selectedVideoIds : []
			};
		}
		case Actions.OPEN_NEW_VIDEO_DIALOG: {
			return {
				...state,
				videoDialog : {
					type  : 'new',
					props : {
						open : true
					},
					data  : null
				}
			};
		}
		case Actions.CLOSE_NEW_VIDEO_DIALOG: {
			return {
				...state,
				videoDialog : {
					type  : 'new',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.OPEN_EDIT_VIDEO_DIALOG: {
			return {
				...state,
				videoDialog : {
					type  : 'edit',
					props : {
						open : true
					},
					data  : action.data
				}
			};
		}
		case Actions.CLOSE_EDIT_VIDEO_DIALOG: {
			return {
				...state,
				videoDialog : {
					type  : 'edit',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.GET_DATA_TO_USE: {
			return {
				...state,
				brands : [ ...action.payload ]
			};
		}
		default: {
			return state;
		}
	}
};

export default videosReducer;
