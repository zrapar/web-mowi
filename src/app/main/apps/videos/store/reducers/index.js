import { combineReducers } from 'redux';
import videos from './videos.reducer';

const reducer = combineReducers({
	videos
});

export default reducer;
