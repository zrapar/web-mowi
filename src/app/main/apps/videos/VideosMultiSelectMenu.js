import React, { useState } from 'react';
import { Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList } from '@material-ui/core';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';

const VideosMultiSelectMenu = (props) => {
	const dispatch = useDispatch();
	const selectedVideoIds = useSelector(({ videos: { videos } }) => videos.selectedVideoIds);

	const [ anchorEl, setAnchorEl ] = useState(null);

	function openSelectedVideoMenu(event) {
		setAnchorEl(event.currentTarget);
	}

	function closeSelectedVideosMenu() {
		setAnchorEl(null);
	}

	return (
		<React.Fragment>
			<IconButton
				className='p-0'
				aria-owns={anchorEl ? 'selectedVideosMenu' : null}
				aria-haspopup='true'
				onClick={openSelectedVideoMenu}
			>
				<Icon>more_horiz</Icon>
			</IconButton>
			<Menu
				id='selectedVideosMenu'
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={closeSelectedVideosMenu}
			>
				<MenuList>
					<MenuItem
						onClick={() => {
							dispatch(Actions.removeVideos(selectedVideoIds));
							closeSelectedVideosMenu();
						}}
					>
						<ListItemIcon className='min-w-40'>
							<Icon>delete</Icon>
						</ListItemIcon>
						<ListItemText primary='Remove' />
					</MenuItem>
				</MenuList>
			</Menu>
		</React.Fragment>
	);
};

export default VideosMultiSelectMenu;
