import React, { useEffect, useCallback, useRef, useState } from 'react';
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	Icon,
	IconButton,
	Typography,
	Toolbar,
	AppBar,
	InputAdornment,
	MenuItem
} from '@material-ui/core';
import Formsy from 'formsy-react';
import { TextFieldFormsy, SelectFormsy } from '@fuse';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';
import jsonToFormData from 'json-form-data';

const defaultFormState = {
	name       : '',
	brand_uuid : '',
	type       : '',
	documents  : null,
	videos     : null
};

const options = {
	initialFormData      : new FormData(),
	showLeafArrayIndexes : true,
	includeNullValues    : false,
	mapping              : function(value) {
		if (typeof value === 'boolean') {
			return +value ? '1' : '0';
		}
		return value;
	}
};

const VideosDialog = (props) => {
	const dispatch = useDispatch();
	const videoDialog = useSelector(({ videos: { videos } }) => videos.videoDialog);
	const brands = useSelector(({ videos: { videos } }) => videos.brands);

	const [ videoData, setData ] = useState(defaultFormState);

	const [ isFormValid, setIsFormValid ] = useState(false);

	const formRef = useRef(null);

	const disableButton = () => {
		setIsFormValid(false);
	};

	const enableButton = () => {
		setIsFormValid(true);
	};

	const initDialog = useCallback(
		() => {
			/**
             * Dialog type: 'edit'
             */
			if (videoDialog.type === 'edit' && videoDialog.data) {
				setData({ ...videoDialog.data });
			}

			/**
             * Dialog type: 'new'
             */
			if (videoDialog.type === 'new') {
				setData({
					...defaultFormState,
					...videoDialog.data
				});
			}
		},
		[ videoDialog.data, videoDialog.type, setData ]
	);

	useEffect(
		() => {
			/**
         * After Dialog Open
         */
			if (videoDialog.props.open) {
				initDialog();
			}
		},
		[ videoDialog.props.open, initDialog ]
	);

	const closeComposeDialog = () => {
		videoDialog.type === 'edit'
			? dispatch(Actions.closeEditVideosDialog())
			: dispatch(Actions.closeNewVideosDialog());
	};

	const handleSubmit = (body) => {
		const newBody = jsonToFormData(body, options);
		if (videoDialog.type === 'new') {
			dispatch(Actions.addVideo(newBody));
		} else {
			dispatch(Actions.updateVideo(newBody, body.uuid));
		}
		closeComposeDialog();
	};

	const handleRemove = () => {
		dispatch(Actions.removeVideo(videoData.uuid));
		closeComposeDialog();
	};

	const uploadDocs = () => {
		document.getElementById('button-docs').click();
	};

	const uploadVideos = () => {
		document.getElementById('button-vids').click();
	};

	const handleUploadChangeDocs = (e) => {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = () => {
			setData({ ...videoData, documents: file });
		};

		reader.onerror = function() {
			console.log('error on load image');
		};
	};

	const handleUploadChangeVids = (e) => {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = () => {
			setData({ ...videoData, videos: file });
		};

		reader.onerror = function() {
			console.log('error on load image');
		};
	};

	return (
		<Dialog
			classes={{
				paper : 'm-24'
			}}
			{...videoDialog.props}
			onClose={closeComposeDialog}
			fullWidth
			maxWidth='xs'
		>
			<AppBar position='static' elevation={1}>
				<Toolbar className='flex w-full'>
					<Typography variant='subtitle1' color='inherit'>
						{videoDialog.type === 'new' ? 'New Video' : 'Edit Video'}
					</Typography>
				</Toolbar>
				<div className='flex flex-col items-center justify-center pb-24'>
					{videoDialog.type === 'edit' && (
						<Typography variant='h6' color='inherit' className='pt-8'>
							{videoData.name}
						</Typography>
					)}
				</div>
			</AppBar>

			<Formsy
				onValidSubmit={handleSubmit}
				onValid={enableButton}
				onInvalid={disableButton}
				ref={formRef}
				className='flex flex-col justify-center w-full'
			>
				<DialogContent classes={{ root: 'p-24' }}>
					<TextFieldFormsy
						className='mb-16 w-full'
						type='text'
						name='name'
						label='Name'
						value={videoData.name}
						validations={{
							minLength : 2
						}}
						validationErrors={{
							minLength : 'The min length is of two characters'
						}}
						InputProps={{
							endAdornment : (
								<InputAdornment position='end'>
									<Icon className='text-20' color='action'>
										email
									</Icon>
								</InputAdornment>
							)
						}}
						variant='outlined'
						required
					/>

					<SelectFormsy
						className='mb-16 w-full'
						name='type'
						label='Type'
						value={videoData.type}
						validations={{
							myCustomIsFiveValidation : function(values, value) {
								const array = [ 'consumer', 'foods' ];
								return array.indexOf(value) >= 0 ? true : 'Must be consumer or foods'; // You can return an error
							}
						}}
					>
						<MenuItem value='consumer'>Consumer Package Goods</MenuItem>
						<MenuItem value='foods'>Food Service</MenuItem>
					</SelectFormsy>
					<SelectFormsy
						className='mb-16 w-full'
						name='brand_uuid'
						label='Brand associate'
						value={videoData.brands ? videoData.brands.uuid : null}
						required
					>
						{brands.map((item, index) => (
							<MenuItem key={index} value={item.uuid}>
								{item.name}
							</MenuItem>
						))}
					</SelectFormsy>

					<TextFieldFormsy
						className='mb-16 w-full'
						type='hidden'
						name='uuid'
						label=''
						value={videoData.uuid ? videoData.uuid : null}
						variant='outlined'
					/>

					<TextFieldFormsy
						className='mb-16 w-full'
						type='hidden'
						name='documents[]'
						label=''
						value={videoData.documents}
						variant='outlined'
						required
					/>

					<TextFieldFormsy
						className='mb-16 w-full'
						type='hidden'
						name='videos[]'
						label=''
						value={videoData.videos}
						variant='outlined'
						required
					/>

					<input
						accept='application/pdf'
						className='hidden'
						id='button-docs'
						type='file'
						multiple
						onChange={handleUploadChangeDocs}
					/>
					<Button
						type='button'
						variant='contained'
						color='primary'
						className='w-full mx-auto mb-16 normal-case'
						aria-label='Select Documents'
						onClick={uploadDocs}
					>
						Select Documents
					</Button>

					<input
						accept='video/3gpp,video/h264,video/mp4,video/quicktime'
						className='hidden'
						id='button-vids'
						type='file'
						multiple
						onChange={handleUploadChangeVids}
					/>
					<Button
						type='button'
						variant='contained'
						color='primary'
						className='w-full mx-auto mb-16 normal-case'
						aria-label='Select Videos'
						onClick={uploadVideos}
					>
						Select Videos
					</Button>
				</DialogContent>

				{videoDialog.type === 'new' ? (
					<DialogActions className='justify-between pl-16'>
						<Button
							type='submit'
							variant='contained'
							color='primary'
							className='w-full mx-auto mt-16 normal-case'
							aria-label='Add Video'
							disabled={!isFormValid}
							value='legacy'
						>
							Add Video
						</Button>
					</DialogActions>
				) : (
					<DialogActions className='justify-between pl-16'>
						<Button
							type='submit'
							variant='contained'
							color='primary'
							className='w-full mx-auto mt-16 normal-case'
							aria-label='Save Video'
							disabled={!isFormValid}
							value='legacy'
						>
							Save Video
						</Button>
						<IconButton onClick={handleRemove}>
							<Icon>delete</Icon>
						</IconButton>
					</DialogActions>
				)}
			</Formsy>
		</Dialog>
	);
};

export default VideosDialog;
