import React from 'react';
import { Redirect } from 'react-router-dom';

export const VideosConfig = {
	settings : {
		layout : {
			config : {}
		}
	},
	routes   : [
		{
			path      : '/apps/videos/:id',
			component : React.lazy(() => import('./Videos'))
		},
		{
			path      : '/apps/videos',
			component : () => <Redirect to='/apps/videos/all' />
		}
	]
};
