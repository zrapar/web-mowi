import axios from 'axios';
import { logoutUser } from 'app/auth/store/actions';
import { showMessage } from 'app/store/actions';

export const GET_LINELISTS = '@@LineLists / GET LINELISTS';
export const GET_DATA_TO_USE = '@@LineLists / GET DATA TO USE';
export const SET_SEARCH_TEXT = '@@LineLists / SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_LINELISTS = '@@LineLists / TOGGLE IN SELECTED LINELISTS';
export const SELECT_ALL_LINELISTS = '@@LineLists / SELECT ALL LINELISTS';
export const DESELECT_ALL_LINELISTS = '@@LineLists / DESELECT ALL LINELISTS';
export const OPEN_NEW_LINELIST_DIALOG = '@@LineLists / OPEN NEW LINELIST DIALOG';
export const CLOSE_NEW_LINELIST_DIALOG = '@@LineLists / CLOSE NEW LINELIST DIALOG';
export const OPEN_EDIT_LINELIST_DIALOG = '@@LineLists / OPEN EDIT LINELIST DIALOG';
export const CLOSE_EDIT_LINELIST_DIALOG = '@@LineLists / CLOSE EDIT LINELIST DIALOG';
export const ADD_LINELIST = '@@LineLists / ADD LINELIST';
export const UPDATE_LINELIST = '@@LineLists / UPDATE LINELIST';
export const REMOVE_LINELIST = '@@LineLists / REMOVE LINELIST';
export const REMOVE_LINELISTS = '@@LineLists / REMOVE LINELISTS';

export const getLineLists = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/linelists', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_LINELISTS,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const getDataToUse = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/brands', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_DATA_TO_USE,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const setSearchText = (event) => ({
	type       : SET_SEARCH_TEXT,
	searchText : event.target.value
});

export const toggleInSelectedLineLists = (lineListId) => ({
	type       : TOGGLE_IN_SELECTED_LINELISTS,
	lineListId
});

export const selectAllLineLists = () => ({
	type : SELECT_ALL_LINELISTS
});

export const deSelectAllLineLists = () => ({
	type : DESELECT_ALL_LINELISTS
});

export const openNewLineListsDialog = () => ({
	type : OPEN_NEW_LINELIST_DIALOG
});

export const closeNewLineListsDialog = () => ({
	type : CLOSE_NEW_LINELIST_DIALOG
});

export const openEditLineListsDialog = (data) => ({
	type : OPEN_EDIT_LINELIST_DIALOG,
	data
});

export const closeEditLineListsDialog = () => ({
	type : CLOSE_EDIT_LINELIST_DIALOG
});

export const addLineList = (newLineList) => async (dispatch) => {
	try {
		const res = await axios.post('/linelists', newLineList, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : ADD_LINELIST
				})
			]).then(() => dispatch(getLineLists()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const updateLineList = (lineList, uuid) => async (dispatch) => {
	try {
		const res = await axios.post(`/linelists/${uuid}`, lineList, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : UPDATE_LINELIST
				})
			]).then(() => dispatch(getLineLists()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeLineList = (lineListId) => async (dispatch) => {
	try {
		const res = await axios.delete(`/linelists/${lineListId}`);
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : REMOVE_LINELIST
				})
			]).then(() => dispatch(getLineLists()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeLineLists = (lineListIds) => (dispatch) => {
	let arrayPromise = [];
	lineListIds.forEach((uuid) => {
		const req = axios.delete(`/linelists/${uuid}`);
		arrayPromise.push(req);
	});

	arrayPromise.push(
		dispatch({
			type : REMOVE_LINELISTS
		})
	);

	Promise.all(arrayPromise).then(() => dispatch(getLineLists()));
	dispatch(
		showMessage({
			message : 'Successfully deleted',
			variant : 'success'
		})
	);
};
