import { combineReducers } from 'redux';
import linelists from './linelists.reducer';

const reducer = combineReducers({
	linelists
});

export default reducer;
