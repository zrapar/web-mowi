import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
	entities            : null,
	searchText          : '',
	selectedLineListIds : [],
	routeParams         : {},
	brands              : [],
	lineListDialog      : {
		type  : 'new',
		props : {
			open : false
		},
		data  : null
	}
};

const lineListsReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_LINELISTS: {
			return {
				...state,
				entities    : _.keyBy(action.payload, 'uuid'),
				routeParams : action.routeParams
			};
		}
		case Actions.SET_SEARCH_TEXT: {
			return {
				...state,
				searchText : action.searchText
			};
		}
		case Actions.TOGGLE_IN_SELECTED_LINELISTS: {
			const linelistId = action.linelistId;

			let selectedLineListIds = [ ...state.selectedLineListIds ];

			if (selectedLineListIds.find((id) => id === linelistId) !== undefined) {
				selectedLineListIds = selectedLineListIds.filter((id) => id !== linelistId);
			} else {
				selectedLineListIds = [ ...selectedLineListIds, linelistId ];
			}

			return {
				...state,
				selectedLineListIds : selectedLineListIds
			};
		}
		case Actions.SELECT_ALL_LINELISTS: {
			const arr = Object.keys(state.entities).map((k) => state.entities[k]);

			const selectedLineListIds = arr.map((linelist) => linelist.id);

			return {
				...state,
				selectedLineListIds : selectedLineListIds
			};
		}
		case Actions.DESELECT_ALL_LINELISTS: {
			return {
				...state,
				selectedLineListIds : []
			};
		}
		case Actions.OPEN_NEW_LINELIST_DIALOG: {
			return {
				...state,
				lineListDialog : {
					type  : 'new',
					props : {
						open : true
					},
					data  : null
				}
			};
		}
		case Actions.CLOSE_NEW_LINELIST_DIALOG: {
			return {
				...state,
				lineListDialog : {
					type  : 'new',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.OPEN_EDIT_LINELIST_DIALOG: {
			return {
				...state,
				lineListDialog : {
					type  : 'edit',
					props : {
						open : true
					},
					data  : action.data
				}
			};
		}
		case Actions.CLOSE_EDIT_LINELIST_DIALOG: {
			return {
				...state,
				lineListDialog : {
					type  : 'edit',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.GET_DATA_TO_USE: {
			return {
				...state,
				brands : [ ...action.payload ]
			};
		}
		default: {
			return state;
		}
	}
};

export default lineListsReducer;
