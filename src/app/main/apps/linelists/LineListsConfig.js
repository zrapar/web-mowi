import React from 'react';
import { Redirect } from 'react-router-dom';

export const LineListsConfig = {
	settings : {
		layout : {
			config : {}
		}
	},
	routes   : [
		{
			path      : '/apps/linelists/:id',
			component : React.lazy(() => import('./LineLists'))
		},
		{
			path      : '/apps/linelists',
			component : () => <Redirect to='/apps/linelists/all' />
		}
	]
};
