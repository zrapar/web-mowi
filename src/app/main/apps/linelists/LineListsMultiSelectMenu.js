import React, { useState } from 'react';
import { Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList } from '@material-ui/core';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';

const LineListsMultiSelectMenu = (props) => {
	const dispatch = useDispatch();
	const selectedLineListIds = useSelector(({ linelists: { linelists } }) => linelists.selectedLineListIds);

	const [ anchorEl, setAnchorEl ] = useState(null);

	function openSelectedLineListMenu(event) {
		setAnchorEl(event.currentTarget);
	}

	function closeSelectedLineListsMenu() {
		setAnchorEl(null);
	}

	return (
		<React.Fragment>
			<IconButton
				className='p-0'
				aria-owns={anchorEl ? 'selectedLineListsMenu' : null}
				aria-haspopup='true'
				onClick={openSelectedLineListMenu}
			>
				<Icon>more_horiz</Icon>
			</IconButton>
			<Menu
				id='selectedLineListsMenu'
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={closeSelectedLineListsMenu}
			>
				<MenuList>
					<MenuItem
						onClick={() => {
							dispatch(Actions.removeLineLists(selectedLineListIds));
							closeSelectedLineListsMenu();
						}}
					>
						<ListItemIcon className='min-w-40'>
							<Icon>delete</Icon>
						</ListItemIcon>
						<ListItemText primary='Remove' />
					</MenuItem>
				</MenuList>
			</Menu>
		</React.Fragment>
	);
};

export default LineListsMultiSelectMenu;
