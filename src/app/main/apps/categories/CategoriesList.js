import React, { useEffect, useState } from 'react';
import { Checkbox, Icon, IconButton, Typography } from '@material-ui/core';
import { FuseUtils, FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from 'react-table';
import * as Actions from './store/actions';
import CategoriesMultiSelectMenu from './CategoriesMultiSelectMenu';

const CategoriesList = (props) => {
	const dispatch = useDispatch();
	const categories = useSelector(({ categories: { categories } }) => categories.entities);
	const selectedCategoryIds = useSelector(({ categories: { categories } }) => categories.selectedCategoryIds);
	const searchText = useSelector(({ categories: { categories } }) => categories.searchText);

	const [ filteredData, setFilteredData ] = useState(null);

	useEffect(
		() => {
			function getFilteredArray(entities, searchText) {
				const arr = Object.keys(entities).map((id) => entities[id]);
				if (searchText.length === 0) {
					return arr;
				}
				return FuseUtils.filterArrayByString(arr, searchText);
			}

			if (categories) {
				setFilteredData(getFilteredArray(categories, searchText));
			}
		},
		[ categories, searchText ]
	);

	if (!filteredData) {
		return null;
	}

	if (filteredData.length === 0) {
		return (
			<div className='flex flex-1 items-center justify-center h-full'>
				<Typography color='textSecondary' variant='h5'>
					There are no categories!
				</Typography>
			</div>
		);
	}

	return (
		<FuseAnimate animation='transition.slideUpIn' delay={300}>
			<ReactTable
				className='-striped -highlight h-full sm:rounded-16 overflow-hidden'
				getTrProps={(state, rowInfo, column) => {
					return {
						className : 'cursor-pointer',
						onClick   : (e, handleOriginal) => {
							if (rowInfo) {
								dispatch(Actions.openEditCategoriesDialog(rowInfo.original));
							}
						}
					};
				}}
				data={filteredData}
				columns={[
					{
						Header    : () => (
							<Checkbox
								onClick={(event) => {
									event.stopPropagation();
								}}
								onChange={(event) => {
									event.target.checked
										? dispatch(Actions.selectAllCategories())
										: dispatch(Actions.deSelectAllCategories());
								}}
								checked={
									selectedCategoryIds.length === Object.keys(categories).length &&
									selectedCategoryIds.length > 0
								}
								indeterminate={
									selectedCategoryIds.length !== Object.keys(categories).length &&
									selectedCategoryIds.length > 0
								}
							/>
						),
						accessor  : '',
						Cell      : (row) => {
							return (
								<Checkbox
									onClick={(event) => {
										event.stopPropagation();
									}}
									checked={selectedCategoryIds.includes(row.value.id)}
									onChange={() => dispatch(Actions.toggleInSelectedCategories(row.value.id))}
								/>
							);
						},
						className : 'justify-center',
						sortable  : false,
						width     : 64
					},
					{
						Header     : 'Name',
						accessor   : 'name',
						filterable : true,
						className  : 'font-bold'
					},
					{
						Header     : 'Product Associate',
						accessor   : 'products',
            Cell: (row) => (
              <div className='flex items-center'>
                <span>{row.value.name}</span>
              </div>
            ),
						filterable : true,
						className  : 'font-bold'
					},
					{
						Header : () => selectedCategoryIds.length > 0 && <CategoriesMultiSelectMenu />,
						width  : 128,
						Cell   : (row) => (
							<div className='flex items-center'>
								<IconButton
									onClick={(ev) => {
										ev.stopPropagation();
										dispatch(Actions.removeCategory(row.original.uuid));
									}}
								>
									<Icon>delete</Icon>
								</IconButton>
							</div>
						)
					}
				]}
				defaultPageSize={10}
				noDataText='No categories found'
			/>
		</FuseAnimate>
	);
};

export default CategoriesList;
