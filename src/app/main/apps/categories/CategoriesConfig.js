import React from 'react';
import { Redirect } from 'react-router-dom';

export const CategoriesConfig = {
	settings : {
		layout : {
			config : {}
		}
	},
	routes   : [
		{
			path      : '/apps/categories/:id',
			component : React.lazy(() => import('./Categories'))
		},
		{
			path      : '/apps/categories',
			component : () => <Redirect to='/apps/categories/all' />
		}
	]
};
