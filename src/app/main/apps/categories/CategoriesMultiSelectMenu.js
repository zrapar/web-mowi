import React, { useState } from 'react';
import { Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList } from '@material-ui/core';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';

const CategoriesMultiSelectMenu = (props) => {
	const dispatch = useDispatch();
	const selectedCategoryIds = useSelector(({ categories: { categories } }) => categories.selectedCategoryIds);

	const [ anchorEl, setAnchorEl ] = useState(null);

	function openSelectedCategoryMenu(event) {
		setAnchorEl(event.currentTarget);
	}

	function closeSelectedCategoriesMenu() {
		setAnchorEl(null);
	}

	return (
		<React.Fragment>
			<IconButton
				className='p-0'
				aria-owns={anchorEl ? 'selectedCategoriesMenu' : null}
				aria-haspopup='true'
				onClick={openSelectedCategoryMenu}
			>
				<Icon>more_horiz</Icon>
			</IconButton>
			<Menu
				id='selectedCategoriesMenu'
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={closeSelectedCategoriesMenu}
			>
				<MenuList>
					<MenuItem
						onClick={() => {
							dispatch(Actions.removeCategories(selectedCategoryIds));
							closeSelectedCategoriesMenu();
						}}
					>
						<ListItemIcon className='min-w-40'>
							<Icon>delete</Icon>
						</ListItemIcon>
						<ListItemText primary='Remove' />
					</MenuItem>
				</MenuList>
			</Menu>
		</React.Fragment>
	);
};

export default CategoriesMultiSelectMenu;
