import React, { useEffect, useCallback, useRef, useState } from 'react';
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	Icon,
	IconButton,
	Typography,
	Toolbar,
	AppBar,
	InputAdornment,
	MenuItem
} from '@material-ui/core';
import Formsy from 'formsy-react';
import { TextFieldFormsy, SelectFormsy } from '@fuse';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';
import jsonToFormData from 'json-form-data';

const defaultFormState = {
	name         : '',
	product_uuid : ''
};

const options = {
	initialFormData      : new FormData(),
	showLeafArrayIndexes : true,
	includeNullValues    : false,
	mapping              : function(value) {
		if (typeof value === 'boolean') {
			return +value ? '1' : '0';
		}
		return value;
	}
};

const CategoriesDialog = (props) => {
	const dispatch = useDispatch();
	const categoryDialog = useSelector(({ categories: { categories } }) => categories.categoryDialog);
	const products = useSelector(({ categories: { categories } }) => categories.products);

	const [ categoryData, setData ] = useState(defaultFormState);

	const [ isFormValid, setIsFormValid ] = useState(false);

	const formRef = useRef(null);

	const disableButton = () => {
		setIsFormValid(false);
	};

	const enableButton = () => {
		setIsFormValid(true);
	};

	const initDialog = useCallback(
		() => {
			/**
             * Dialog type: 'edit'
             */
			if (categoryDialog.type === 'edit' && categoryDialog.data) {
				setData({ ...categoryDialog.data });
			}

			/**
             * Dialog type: 'new'
             */
			if (categoryDialog.type === 'new') {
				setData({
					...defaultFormState,
					...categoryDialog.data
				});
			}
		},
		[ categoryDialog.data, categoryDialog.type, setData ]
	);

	useEffect(
		() => {
			/**
         * After Dialog Open
         */
			if (categoryDialog.props.open) {
				initDialog();
			}
		},
		[ categoryDialog.props.open, initDialog ]
	);

	const closeComposeDialog = () => {
		categoryDialog.type === 'edit'
			? dispatch(Actions.closeEditCategoriesDialog())
			: dispatch(Actions.closeNewCategoriesDialog());
	};

	const handleSubmit = (body) => {
		const newBody = jsonToFormData(body, options);
		if (categoryDialog.type === 'new') {
			dispatch(Actions.addCategory(newBody));
		} else {
			dispatch(Actions.updateCategory(newBody, body.uuid));
		}
		closeComposeDialog();
	};

	const handleRemove = () => {
		dispatch(Actions.removeCategory(categoryData.uuid));
		closeComposeDialog();
	};

	return (
		<Dialog
			classes={{
				paper : 'm-24'
			}}
			{...categoryDialog.props}
			onClose={closeComposeDialog}
			fullWidth
			maxWidth='xs'
		>
			<AppBar position='static' elevation={1}>
				<Toolbar className='flex w-full'>
					<Typography variant='subtitle1' color='inherit'>
						{categoryDialog.type === 'new' ? 'New Category' : 'Edit Category'}
					</Typography>
				</Toolbar>
				<div className='flex flex-col items-center justify-center pb-24'>
					{categoryDialog.type === 'edit' && (
						<Typography variant='h6' color='inherit' className='pt-8'>
							{categoryData.name}
						</Typography>
					)}
				</div>
			</AppBar>

			<Formsy
				onValidSubmit={handleSubmit}
				onValid={enableButton}
				onInvalid={disableButton}
				ref={formRef}
				className='flex flex-col justify-center w-full'
			>
				<DialogContent classes={{ root: 'p-24' }}>
					<TextFieldFormsy
						className='mb-16 w-full'
						type='text'
						name='name'
						label='Name'
						value={categoryData.name}
						validations={{
							minLength : 2
						}}
						validationErrors={{
							minLength : 'The min length is of two characters'
						}}
						InputProps={{
							endAdornment : (
								<InputAdornment position='end'>
									<Icon className='text-20' color='action'>
										email
									</Icon>
								</InputAdornment>
							)
						}}
						variant='outlined'
						required
					/>
					<SelectFormsy
						className='mb-16 w-full'
						name='product_uuid'
						label='Product associate'
						value={categoryData.products ? categoryData.products.uuid : null}
						required
					>
						{products.map((item, index) => (
							<MenuItem key={index} value={item.uuid}>
								{item.name}
							</MenuItem>
						))}
					</SelectFormsy>

					<TextFieldFormsy
						className='mb-16 w-full'
						type='hidden'
						name='uuid'
						label=''
						value={categoryData.uuid ? categoryData.uuid : null}
						variant='outlined'
					/>
				</DialogContent>

				{categoryDialog.type === 'new' ? (
					<DialogActions className='justify-between pl-16'>
						<Button
							type='submit'
							variant='contained'
							color='primary'
							className='w-full mx-auto mt-16 normal-case'
							aria-label='Add Category'
							disabled={!isFormValid}
							value='legacy'
						>
							Add Category
						</Button>
					</DialogActions>
				) : (
					<DialogActions className='justify-between pl-16'>
						<Button
							type='submit'
							variant='contained'
							color='primary'
							className='w-full mx-auto mt-16 normal-case'
							aria-label='Save Category'
							disabled={!isFormValid}
							value='legacy'
						>
							Save Category
						</Button>
						<IconButton onClick={handleRemove}>
							<Icon>delete</Icon>
						</IconButton>
					</DialogActions>
				)}
			</Formsy>
		</Dialog>
	);
};

export default CategoriesDialog;
