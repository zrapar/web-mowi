import { combineReducers } from 'redux';
import categories from './categories.reducer';

const reducer = combineReducers({
	categories
});

export default reducer;
