import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
	entities            : null,
	searchText          : '',
	selectedCategoryIds : [],
	routeParams         : {},
	products            : [],
	categoryDialog      : {
		type  : 'new',
		props : {
			open : false
		},
		data  : null
	}
};

const categoriesReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_CATEGORIES: {
			return {
				...state,
				entities    : _.keyBy(action.payload, 'uuid'),
				routeParams : action.routeParams
			};
		}
		case Actions.SET_SEARCH_TEXT: {
			return {
				...state,
				searchText : action.searchText
			};
		}
		case Actions.TOGGLE_IN_SELECTED_CATEGORIES: {
			const categoryId = action.categoryId;

			let selectedCategoryIds = [ ...state.selectedCategoryIds ];

			if (selectedCategoryIds.find((id) => id === categoryId) !== undefined) {
				selectedCategoryIds = selectedCategoryIds.filter((id) => id !== categoryId);
			} else {
				selectedCategoryIds = [ ...selectedCategoryIds, categoryId ];
			}

			return {
				...state,
				selectedCategoryIds : selectedCategoryIds
			};
		}
		case Actions.SELECT_ALL_CATEGORIES: {
			const arr = Object.keys(state.entities).map((k) => state.entities[k]);

			const selectedCategoryIds = arr.map((category) => category.id);

			return {
				...state,
				selectedCategoryIds : selectedCategoryIds
			};
		}
		case Actions.DESELECT_ALL_CATEGORIES: {
			return {
				...state,
				selectedCategoryIds : []
			};
		}
		case Actions.OPEN_NEW_CATEGORY_DIALOG: {
			return {
				...state,
				categoryDialog : {
					type  : 'new',
					props : {
						open : true
					},
					data  : null
				}
			};
		}
		case Actions.CLOSE_NEW_CATEGORY_DIALOG: {
			return {
				...state,
				categoryDialog : {
					type  : 'new',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.OPEN_EDIT_CATEGORY_DIALOG: {
			return {
				...state,
				categoryDialog : {
					type  : 'edit',
					props : {
						open : true
					},
					data  : action.data
				}
			};
		}
		case Actions.CLOSE_EDIT_CATEGORY_DIALOG: {
			return {
				...state,
				categoryDialog : {
					type  : 'edit',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.GET_DATA_TO_USE: {
			return {
				...state,
				products : [ ...action.payload ]
			};
		}
		default: {
			return state;
		}
	}
};

export default categoriesReducer;
