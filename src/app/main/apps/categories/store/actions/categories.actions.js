import axios from 'axios';
import { logoutUser } from 'app/auth/store/actions';
import { showMessage } from 'app/store/actions';

export const GET_CATEGORIES = '@@Categories / GET CATEGORIES';
export const GET_DATA_TO_USE = '@@Categories / GET DATA TO USE';
export const SET_SEARCH_TEXT = '@@Categories / SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_CATEGORIES = '@@Categories / TOGGLE IN SELECTED CATEGORIES';
export const SELECT_ALL_CATEGORIES = '@@Categories / SELECT ALL CATEGORIES';
export const DESELECT_ALL_CATEGORIES = '@@Categories / DESELECT ALL CATEGORIES';
export const OPEN_NEW_CATEGORY_DIALOG = '@@Categories / OPEN NEW CATEGORY DIALOG';
export const CLOSE_NEW_CATEGORY_DIALOG = '@@Categories / CLOSE NEW CATEGORY DIALOG';
export const OPEN_EDIT_CATEGORY_DIALOG = '@@Categories / OPEN EDIT CATEGORY DIALOG';
export const CLOSE_EDIT_CATEGORY_DIALOG = '@@Categories / CLOSE EDIT CATEGORY DIALOG';
export const ADD_CATEGORY = '@@Categories / ADD CATEGORY';
export const UPDATE_CATEGORY = '@@Categories / UPDATE CATEGORY';
export const REMOVE_CATEGORY = '@@Categories / REMOVE CATEGORY';
export const REMOVE_CATEGORIES = '@@Categories / REMOVE CATEGORIES';

export const getCategories = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/categories', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_CATEGORIES,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const getDataToUse = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/products', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_DATA_TO_USE,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const setSearchText = (event) => ({
	type       : SET_SEARCH_TEXT,
	searchText : event.target.value
});

export const toggleInSelectedCategories = (categoriesId) => ({
	type         : TOGGLE_IN_SELECTED_CATEGORIES,
	categoriesId
});

export const selectAllCategories = () => ({
	type : SELECT_ALL_CATEGORIES
});

export const deSelectAllCategories = () => ({
	type : DESELECT_ALL_CATEGORIES
});

export const openNewCategoriesDialog = () => ({
	type : OPEN_NEW_CATEGORY_DIALOG
});

export const closeNewCategoriesDialog = () => ({
	type : CLOSE_NEW_CATEGORY_DIALOG
});

export const openEditCategoriesDialog = (data) => ({
	type : OPEN_EDIT_CATEGORY_DIALOG,
	data
});

export const closeEditCategoriesDialog = () => ({
	type : CLOSE_EDIT_CATEGORY_DIALOG
});

export const addCategory = (newCategory) => async (dispatch) => {
	try {
		const res = await axios.post('/categories', newCategory, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : ADD_CATEGORY
				})
			]).then(() => dispatch(getCategories()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const updateCategory = (categories, uuid) => async (dispatch) => {
	try {
		const res = await axios.post(`/categories/${uuid}`, categories, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : UPDATE_CATEGORY
				})
			]).then(() => dispatch(getCategories()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeCategory = (categoriesId) => async (dispatch) => {
	try {
		const res = await axios.delete(`/categories/${categoriesId}`);
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : REMOVE_CATEGORY
				})
			]).then(() => dispatch(getCategories()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeCategories = (categoriesIds) => (dispatch) => {
	let arrayPromise = [];
	categoriesIds.forEach((uuid) => {
		const req = axios.delete(`/categories/${uuid}`);
		arrayPromise.push(req);
	});

	arrayPromise.push(
		dispatch({
			type : REMOVE_CATEGORIES
		})
	);

	Promise.all(arrayPromise).then(() => dispatch(getCategories()));
	dispatch(
		showMessage({
			message : 'Successfully deleted',
			variant : 'success'
		})
	);
};
