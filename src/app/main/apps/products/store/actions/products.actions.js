import axios from 'axios';
import { logoutUser } from 'app/auth/store/actions';
import { showMessage } from 'app/store/actions';

export const GET_PRODUCTS = '@@Products / GET PRODUCTS';
export const GET_DATA_TO_USE = '@@Products / GET DATA TO USE';
export const SET_SEARCH_TEXT = '@@Products / SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_PRODUCTS = '@@Products / TOGGLE IN SELECTED PRODUCTS';
export const SELECT_ALL_PRODUCTS = '@@Products / SELECT ALL PRODUCTS';
export const DESELECT_ALL_PRODUCTS = '@@Products / DESELECT ALL PRODUCTS';
export const OPEN_NEW_PRODUCT_DIALOG = '@@Products / OPEN NEW PRODUCT DIALOG';
export const CLOSE_NEW_PRODUCT_DIALOG = '@@Products / CLOSE NEW PRODUCT DIALOG';
export const OPEN_EDIT_PRODUCT_DIALOG = '@@Products / OPEN EDIT PRODUCT DIALOG';
export const CLOSE_EDIT_PRODUCT_DIALOG = '@@Products / CLOSE EDIT PRODUCT DIALOG';
export const ADD_PRODUCT = '@@Products / ADD PRODUCT';
export const UPDATE_PRODUCT = '@@Products / UPDATE PRODUCT';
export const REMOVE_PRODUCT = '@@Products / REMOVE PRODUCT';
export const REMOVE_PRODUCTS = '@@Products / REMOVE PRODUCTS';

export const getProducts = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/products', {
			params : routeParams
		});

		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_PRODUCTS,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const getDataToUse = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/brands', {
			params : routeParams
		});

		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_DATA_TO_USE,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const setSearchText = (event) => ({
	type       : SET_SEARCH_TEXT,
	searchText : event.target.value
});

export const toggleInSelectedProducts = (presentationId) => ({
	type           : TOGGLE_IN_SELECTED_PRODUCTS,
	presentationId
});

export const selectAllProducts = () => ({
	type : SELECT_ALL_PRODUCTS
});

export const deSelectAllProducts = () => ({
	type : DESELECT_ALL_PRODUCTS
});

export const openNewProductsDialog = () => ({
	type : OPEN_NEW_PRODUCT_DIALOG
});

export const closeNewProductsDialog = () => ({
	type : CLOSE_NEW_PRODUCT_DIALOG
});

export const openEditProductsDialog = (data) => ({
	type : OPEN_EDIT_PRODUCT_DIALOG,
	data
});

export const closeEditProductsDialog = () => ({
	type : CLOSE_EDIT_PRODUCT_DIALOG
});

export const addProduct = (newProduct) => async (dispatch) => {
	try {
		const res = await axios.post('/products', newProduct, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : ADD_PRODUCT
				})
			]).then(() => dispatch(getProducts()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const updateProduct = (presentation, uuid) => async (dispatch) => {
	try {
		const res = await axios.post(`/products/${uuid}`, presentation, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;
		if (success) {
			Promise.all([
				dispatch({
					type : UPDATE_PRODUCT
				})
			]).then(() => dispatch(getProducts()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeProduct = (presentationId) => async (dispatch) => {
	try {
		const res = await axios.delete(`/products/${presentationId}`);
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : REMOVE_PRODUCT
				})
			]).then(() => dispatch(getProducts()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeProducts = (presentationIds) => (dispatch) => {
	let arrayPromise = [];
	presentationIds.forEach((uuid) => {
		const req = axios.delete(`/products/${uuid}`);
		arrayPromise.push(req);
	});

	arrayPromise.push(
		dispatch({
			type : REMOVE_PRODUCTS
		})
	);

	Promise.all(arrayPromise).then(() => dispatch(getProducts()));
	dispatch(
		showMessage({
			message : 'Successfully deleted',
			variant : 'success'
		})
	);
};
