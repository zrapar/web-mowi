import React from 'react';
import { Redirect } from 'react-router-dom';

export const ProductsConfig = {
	settings : {
		layout : {
			config : {}
		}
	},
	routes   : [
		{
			path      : '/apps/products/:id',
			component : React.lazy(() => import('./Products'))
		},
		{
			path      : '/apps/products',
			component : () => <Redirect to='/apps/products/all' />
		}
	]
};
