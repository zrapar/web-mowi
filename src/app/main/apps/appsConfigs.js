import { ProjectDashboardAppConfig } from './dashboards/ProjectDashboardAppConfig';
import { MailAppConfig } from './mail/MailAppConfig';
import { BrandsConfig } from './brands/BrandsConfig';
import { LineListsConfig } from './linelists/LineListsConfig';
import { PresentationsConfig } from './presentations/PresentationsConfig';
import { MarketingsConfig } from './marketing/MarketingsConfig';
import { ProductsConfig } from './products/ProductsConfig';
import { CategoriesConfig } from './categories/CategoriesConfig';
import { VideosConfig } from './videos/VideosConfig';

export const appsConfigs = [
	ProjectDashboardAppConfig,
	MailAppConfig,
	BrandsConfig,
	LineListsConfig,
	PresentationsConfig,
	MarketingsConfig,
	ProductsConfig,
	CategoriesConfig,
	VideosConfig
];
