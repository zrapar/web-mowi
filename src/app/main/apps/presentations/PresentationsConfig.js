import React from 'react';
import { Redirect } from 'react-router-dom';

export const PresentationsConfig = {
	settings : {
		layout : {
			config : {}
		}
	},
	routes   : [
		{
			path      : '/apps/presentations/:id',
			component : React.lazy(() => import('./Presentations'))
		},
		{
			path      : '/apps/presentations',
			component : () => <Redirect to='/apps/presentations/all' />
		}
	]
};
