import { combineReducers } from 'redux';
import presentations from './presentations.reducer';

const reducer = combineReducers({
	presentations
});

export default reducer;
