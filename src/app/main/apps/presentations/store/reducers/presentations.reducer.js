import * as Actions from '../actions';
import _ from '@lodash';

const initialState = {
	entities            : null,
	searchText          : '',
	selectedPresentationIds : [],
	routeParams         : {},
	brands              : [],
	presentationDialog      : {
		type  : 'new',
		props : {
			open : false
		},
		data  : null
	}
};

const presentationsReducer = function(state = initialState, action) {
	switch (action.type) {
		case Actions.GET_PRESENTATIONS: {
			return {
				...state,
				entities    : _.keyBy(action.payload, 'uuid'),
				routeParams : action.routeParams
			};
		}
		case Actions.SET_SEARCH_TEXT: {
			return {
				...state,
				searchText : action.searchText
			};
		}
		case Actions.TOGGLE_IN_SELECTED_PRESENTATIONS: {
			const presentationId = action.presentationId;

			let selectedPresentationIds = [ ...state.selectedPresentationIds ];

			if (selectedPresentationIds.find((id) => id === presentationId) !== undefined) {
				selectedPresentationIds = selectedPresentationIds.filter((id) => id !== presentationId);
			} else {
				selectedPresentationIds = [ ...selectedPresentationIds, presentationId ];
			}

			return {
				...state,
				selectedPresentationIds : selectedPresentationIds
			};
		}
		case Actions.SELECT_ALL_PRESENTATIONS: {
			const arr = Object.keys(state.entities).map((k) => state.entities[k]);

			const selectedPresentationIds = arr.map((presentation) => presentation.id);

			return {
				...state,
				selectedPresentationIds : selectedPresentationIds
			};
		}
		case Actions.DESELECT_ALL_PRESENTATIONS: {
			return {
				...state,
				selectedPresentationIds : []
			};
		}
		case Actions.OPEN_NEW_PRESENTATION_DIALOG: {
			return {
				...state,
				presentationDialog : {
					type  : 'new',
					props : {
						open : true
					},
					data  : null
				}
			};
		}
		case Actions.CLOSE_NEW_PRESENTATION_DIALOG: {
			return {
				...state,
				presentationDialog : {
					type  : 'new',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.OPEN_EDIT_PRESENTATION_DIALOG: {
			return {
				...state,
				presentationDialog : {
					type  : 'edit',
					props : {
						open : true
					},
					data  : action.data
				}
			};
		}
		case Actions.CLOSE_EDIT_PRESENTATION_DIALOG: {
			return {
				...state,
				presentationDialog : {
					type  : 'edit',
					props : {
						open : false
					},
					data  : null
				}
			};
		}
		case Actions.GET_DATA_TO_USE: {
			return {
				...state,
				brands : [ ...action.payload ]
			};
		}
		default: {
			return state;
		}
	}
};

export default presentationsReducer;
