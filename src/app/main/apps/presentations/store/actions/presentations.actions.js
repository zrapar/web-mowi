import axios from 'axios';
import { logoutUser } from 'app/auth/store/actions';
import { showMessage } from 'app/store/actions';

export const GET_PRESENTATIONS = '@@Presentations / GET PRESENTATIONS';
export const GET_DATA_TO_USE = '@@Presentations / GET DATA TO USE';
export const SET_SEARCH_TEXT = '@@Presentations / SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_PRESENTATIONS = '@@Presentations / TOGGLE IN SELECTED PRESENTATIONS';
export const SELECT_ALL_PRESENTATIONS = '@@Presentations / SELECT ALL PRESENTATIONS';
export const DESELECT_ALL_PRESENTATIONS = '@@Presentations / DESELECT ALL PRESENTATIONS';
export const OPEN_NEW_PRESENTATION_DIALOG = '@@Presentations / OPEN NEW PRESENTATION DIALOG';
export const CLOSE_NEW_PRESENTATION_DIALOG = '@@Presentations / CLOSE NEW PRESENTATION DIALOG';
export const OPEN_EDIT_PRESENTATION_DIALOG = '@@Presentations / OPEN EDIT PRESENTATION DIALOG';
export const CLOSE_EDIT_PRESENTATION_DIALOG = '@@Presentations / CLOSE EDIT PRESENTATION DIALOG';
export const ADD_PRESENTATION = '@@Presentations / ADD PRESENTATION';
export const UPDATE_PRESENTATION = '@@Presentations / UPDATE PRESENTATION';
export const REMOVE_PRESENTATION = '@@Presentations / REMOVE PRESENTATION';
export const REMOVE_PRESENTATIONS = '@@Presentations / REMOVE PRESENTATIONS';

export const getPresentations = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/presentation', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_PRESENTATIONS,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const getDataToUse = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/brands', {
			params : routeParams
		});

		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;

		if (success) {
			dispatch({
				type        : GET_DATA_TO_USE,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const setSearchText = (event) => ({
	type       : SET_SEARCH_TEXT,
	searchText : event.target.value
});

export const toggleInSelectedPresentations = (presentationId) => ({
	type           : TOGGLE_IN_SELECTED_PRESENTATIONS,
	presentationId
});

export const selectAllPresentations = () => ({
	type : SELECT_ALL_PRESENTATIONS
});

export const deSelectAllPresentations = () => ({
	type : DESELECT_ALL_PRESENTATIONS
});

export const openNewPresentationsDialog = () => ({
	type : OPEN_NEW_PRESENTATION_DIALOG
});

export const closeNewPresentationsDialog = () => ({
	type : CLOSE_NEW_PRESENTATION_DIALOG
});

export const openEditPresentationsDialog = (data) => ({
	type : OPEN_EDIT_PRESENTATION_DIALOG,
	data
});

export const closeEditPresentationsDialog = () => ({
	type : CLOSE_EDIT_PRESENTATION_DIALOG
});

export const addPresentation = (newPresentation) => async (dispatch) => {
	try {
		const res = await axios.post('/presentation', newPresentation, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});

		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : ADD_PRESENTATION
				})
			]).then(() => dispatch(getPresentations()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const updatePresentation = (presentation, uuid) => async (dispatch) => {
	try {
		const res = await axios.post(`/presentation/${uuid}`, presentation, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});

		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : UPDATE_PRESENTATION
				})
			]).then(() => dispatch(getPresentations()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removePresentation = (presentationId) => async (dispatch) => {
	try {
		const res = await axios.delete(`/presentation/${presentationId}`);

		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : REMOVE_PRESENTATION
				})
			]).then(() => dispatch(getPresentations()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removePresentations = (presentationIds) => (dispatch) => {
	let arrayPromise = [];
	presentationIds.forEach((uuid) => {
		const req = axios.delete(`/presentation/${uuid}`);
		arrayPromise.push(req);
	});

	arrayPromise.push(
		dispatch({
			type : REMOVE_PRESENTATIONS
		})
	);

	Promise.all(arrayPromise).then(() => dispatch(getPresentations()));
	dispatch(
		showMessage({
			message : 'Successfully deleted',
			variant : 'success'
		})
	);
};
