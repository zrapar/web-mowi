import React, { useEffect, useState } from 'react';
import { Checkbox, Icon, IconButton, Typography } from '@material-ui/core';
import { FuseUtils, FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from 'react-table';
import * as Actions from './store/actions';
import PresentationsMultiSelectMenu from './PresentationsMultiSelectMenu';

const PresentationsList = (props) => {
	const dispatch = useDispatch();
	const presentations = useSelector(({ presentations: { presentations } }) => presentations.entities);
	const selectedPresentationIds = useSelector(
		({ presentations: { presentations } }) => presentations.selectedPresentationIds
	);
	const searchText = useSelector(({ presentations: { presentations } }) => presentations.searchText);

	const [ filteredData, setFilteredData ] = useState(null);

	useEffect(
		() => {
			function getFilteredArray(entities, searchText) {
				const arr = Object.keys(entities).map((id) => entities[id]);
				if (searchText.length === 0) {
					return arr;
				}
				return FuseUtils.filterArrayByString(arr, searchText);
			}

			if (presentations) {
				setFilteredData(getFilteredArray(presentations, searchText));
			}
		},
		[ presentations, searchText ]
	);

	if (!filteredData) {
		return null;
	}

	if (filteredData.length === 0) {
		return (
			<div className='flex flex-1 items-center justify-center h-full'>
				<Typography color='textSecondary' variant='h5'>
					There are no presentations!
				</Typography>
			</div>
		);
	}

	return (
		<FuseAnimate animation='transition.slideUpIn' delay={300}>
			<ReactTable
				className='-striped -highlight h-full sm:rounded-16 overflow-hidden'
				getTrProps={(state, rowInfo, column) => {
					return {
						className : 'cursor-pointer',
						onClick   : (e, handleOriginal) => {
							if (rowInfo) {
								dispatch(Actions.openEditPresentationsDialog(rowInfo.original));
							}
						}
					};
				}}
				data={filteredData}
				columns={[
					{
						Header    : () => (
							<Checkbox
								onClick={(event) => {
									event.stopPropagation();
								}}
								onChange={(event) => {
									event.target.checked
										? dispatch(Actions.selectAllPresentations())
										: dispatch(Actions.deSelectAllPresentations());
								}}
								checked={
									selectedPresentationIds.length === Object.keys(presentations).length &&
									selectedPresentationIds.length > 0
								}
								indeterminate={
									selectedPresentationIds.length !== Object.keys(presentations).length &&
									selectedPresentationIds.length > 0
								}
							/>
						),
						accessor  : '',
						Cell      : (row) => {
							return (
								<Checkbox
									onClick={(event) => {
										event.stopPropagation();
									}}
									checked={selectedPresentationIds.includes(row.value.id)}
									onChange={() => dispatch(Actions.toggleInSelectedPresentations(row.value.id))}
								/>
							);
						},
						className : 'justify-center',
						sortable  : false,
						width     : 64
					},
					{
						Header     : 'Name',
						accessor   : 'name',
						filterable : true,
						className  : 'font-bold'
					},
					{
						Header     : 'Brand Associate',
						accessor   : 'brands',
						Cell       : (row) => (
							<div className='flex items-center'>
								<span>{row.value.name}</span>
							</div>
						),
						filterable : true,
						className  : 'font-bold'
					},
					{
						Header     : 'Type',
						accessor   : 'type',
						Cell       : (row) => (
							<span>{row.value === 'foods' ? 'Food Service' : 'Consumer Package Goods'}</span>
						),
						filterable : true
					},
					{
						Header : () => selectedPresentationIds.length > 0 && <PresentationsMultiSelectMenu />,
						width  : 128,
						Cell   : (row) => (
							<div className='flex items-center'>
								<IconButton
									onClick={(ev) => {
										ev.stopPropagation();
										dispatch(Actions.removePresentation(row.original.uuid));
									}}
								>
									<Icon>delete</Icon>
								</IconButton>
							</div>
						)
					}
				]}
				defaultPageSize={10}
				noDataText='No presentations found'
			/>
		</FuseAnimate>
	);
};

export default PresentationsList;
