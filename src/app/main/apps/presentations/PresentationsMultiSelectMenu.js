import React, { useState } from 'react';
import { Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList } from '@material-ui/core';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';

const PresentationsMultiSelectMenu = (props) => {
	const dispatch = useDispatch();
	const selectedPresentationIds = useSelector(({ presentations: { presentations } }) => presentations.selectedPresentationIds);

	const [ anchorEl, setAnchorEl ] = useState(null);

	function openSelectedPresentationMenu(event) {
		setAnchorEl(event.currentTarget);
	}

	function closeSelectedPresentationsMenu() {
		setAnchorEl(null);
	}

	return (
		<React.Fragment>
			<IconButton
				className='p-0'
				aria-owns={anchorEl ? 'selectedPresentationsMenu' : null}
				aria-haspopup='true'
				onClick={openSelectedPresentationMenu}
			>
				<Icon>more_horiz</Icon>
			</IconButton>
			<Menu
				id='selectedPresentationsMenu'
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={closeSelectedPresentationsMenu}
			>
				<MenuList>
					<MenuItem
						onClick={() => {
							dispatch(Actions.removePresentations(selectedPresentationIds));
							closeSelectedPresentationsMenu();
						}}
					>
						<ListItemIcon className='min-w-40'>
							<Icon>delete</Icon>
						</ListItemIcon>
						<ListItemText primary='Remove' />
					</MenuItem>
				</MenuList>
			</Menu>
		</React.Fragment>
	);
};

export default PresentationsMultiSelectMenu;
