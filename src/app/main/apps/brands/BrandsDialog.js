import React, { useEffect, useCallback, useRef, useState } from 'react';
import {
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	Icon,
	IconButton,
	Typography,
	Toolbar,
	AppBar,
	Avatar,
	InputAdornment
} from '@material-ui/core';
import Formsy from 'formsy-react';
import { TextFieldFormsy } from '@fuse';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';
import { SketchPicker } from 'react-color';
import jsonToFormData from 'json-form-data';

const defaultFormState = {
	name         : '',
	color        : '',
	image        : null,
	imagePreview : null,
	background   : null
};

const options = {
	initialFormData      : new FormData(),
	showLeafArrayIndexes : true,
	includeNullValues    : false,
	mapping              : function(value) {
		if (typeof value === 'boolean') {
			return +value ? '1' : '0';
		}
		return value;
	}
};

const BrandsDialog = (props) => {
	const dispatch = useDispatch();
	const brandDialog = useSelector(({ brands: { brands } }) => brands.brandDialog);

	const [ brandData, setData ] = useState(defaultFormState);

	const [ isFormValid, setIsFormValid ] = useState(false);

	const [ showColorPicker, toggleColorPicker ] = useState(false);

	const formRef = useRef(null);

	const disableButton = () => {
		setIsFormValid(false);
	};

	const enableButton = () => {
		setIsFormValid(true);
	};

	const initDialog = useCallback(
		() => {
			/**
             * Dialog type: 'edit'
             */
			if (brandDialog.type === 'edit' && brandDialog.data) {
				setData({ ...brandDialog.data });
			}

			/**
             * Dialog type: 'new'
             */
			if (brandDialog.type === 'new') {
				setData({
					...defaultFormState,
					...brandDialog.data
				});
			}
		},
		[ brandDialog.data, brandDialog.type, setData ]
	);

	useEffect(
		() => {
			/**
         * After Dialog Open
         */
			if (brandDialog.props.open) {
				initDialog();
			}
		},
		[ brandDialog.props.open, initDialog ]
	);

	const closeComposeDialog = () => {
		brandDialog.type === 'edit'
			? dispatch(Actions.closeEditBrandsDialog())
			: dispatch(Actions.closeNewBrandsDialog());
	};

	const handleSubmit = (body) => {
		const newBody = jsonToFormData(body, options);
		if (brandDialog.type === 'new') {
			dispatch(Actions.addBrand(newBody));
		} else {
			dispatch(Actions.updateBrand(newBody, body.uuid));
		}
		closeComposeDialog();
	};

	const handleRemove = () => {
		dispatch(Actions.removeBrand(brandData.uuid));
		closeComposeDialog();
	};

	const handleChangeComplete = (color) => {
		setData({ ...brandData, color: color.hex });
	};

	const openPicker = (e) => {
		e.preventDefault();
		toggleColorPicker(!showColorPicker);
	};

	const uploadImage = () => {
		document.getElementById('button-file').click();
	};

	const handleUploadChange = (e) => {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = () => {
			setData({ ...brandData, image: file, imagePreview: `data:${file.type};base64,${btoa(reader.result)}` });
		};

		reader.onerror = function() {
			console.log('error on load image');
		};
	};

	const uploadImageBg = () => {
		document.getElementById('button-fileBg').click();
	};

	const handleUploadChangeBg = (e) => {
		const file = e.target.files[0];
		if (!file) {
			return;
		}
		const reader = new FileReader();
		reader.readAsBinaryString(file);

		reader.onload = () => {
			setData({ ...brandData, background: file });
		};

		reader.onerror = function() {
			console.log('error on load image');
		};
	};

	return (
		<Dialog
			classes={{
				paper : 'm-24'
			}}
			{...brandDialog.props}
			onClose={closeComposeDialog}
			fullWidth
			maxWidth='xs'
		>
			<AppBar position='static' elevation={1}>
				<Toolbar className='flex w-full'>
					<Typography variant='subtitle1' color='inherit'>
						{brandDialog.type === 'new' ? 'New Brand' : 'Edit Brand'}
					</Typography>
				</Toolbar>
				<div className='flex flex-col items-center justify-center pb-24'>
					<Avatar
						className='w-96 h-96'
						alt='brand avatar'
						src={brandData.imagePreview ? brandData.imagePreview : brandData.image}
					/>
					{brandDialog.type === 'edit' && (
						<Typography variant='h6' color='inherit' className='pt-8'>
							{brandData.name}
						</Typography>
					)}
				</div>
			</AppBar>

			<Formsy
				onValidSubmit={handleSubmit}
				onValid={enableButton}
				onInvalid={disableButton}
				ref={formRef}
				className='flex flex-col justify-center w-full'
			>
				<DialogContent classes={{ root: 'p-24' }}>
					<TextFieldFormsy
						className='mb-16 w-full'
						type='text'
						name='name'
						label='Name'
						value={brandData.name}
						validations={{
							minLength : 2
						}}
						validationErrors={{
							minLength : 'The min length is of two characters'
						}}
						InputProps={{
							endAdornment : (
								<InputAdornment position='end'>
									<Icon className='text-20' color='action'>
										email
									</Icon>
								</InputAdornment>
							)
						}}
						variant='outlined'
						required
					/>

					<TextFieldFormsy
						className='mb-16 w-full'
						type='hidden'
						name='uuid'
						label=''
						value={brandData.uuid ? brandData.uuid : null}
						variant='outlined'
					/>
					<TextFieldFormsy
						className='mb-16 w-full'
						type='hidden'
						name='color'
						label=''
						value={brandData.color}
						variant='outlined'
						required
					/>
					<Button
						type='button'
						variant='contained'
						color='primary'
						className='w-full mx-auto mb-16 normal-case'
						aria-label='Select Color'
						onClick={openPicker}
					>
						{showColorPicker ? 'Close color picker' : 'Open color picker'}
					</Button>
					{showColorPicker && (
						<SketchPicker
							className='mb-16 w-full'
							color={brandData.color.length === 0 ? '#212121' : brandData.color}
							onChangeComplete={handleChangeComplete}
						/>
					)}
					<TextFieldFormsy
						className='mb-16 w-full'
						type='hidden'
						name='image'
						label=''
						value={brandData.image}
						variant='outlined'
						required
					/>
					<input
						accept='image/*'
						className='hidden'
						id='button-file'
						type='file'
						onChange={handleUploadChange}
					/>
					<Button
						type='button'
						variant='contained'
						color='primary'
						className='w-full mx-auto mb-16 normal-case'
						aria-label='Select Color'
						onClick={uploadImage}
					>
						Upload image
					</Button>

					<TextFieldFormsy
						className='mb-16 w-full'
						type='hidden'
						name='background'
						label=''
						value={brandData.background}
						variant='outlined'
						required
					/>
					<input
						accept='image/*'
						className='hidden'
						id='button-fileBg'
						type='file'
						onChange={handleUploadChangeBg}
					/>
					<Button
						type='button'
						variant='contained'
						color='primary'
						className='w-full mx-auto mb-16 normal-case'
						aria-label='Select Color'
						onClick={uploadImageBg}
					>
						Upload Background
					</Button>
				</DialogContent>

				{brandDialog.type === 'new' ? (
					<DialogActions className='justify-between pl-16'>
						<Button
							type='submit'
							variant='contained'
							color='primary'
							className='w-full mx-auto mt-16 normal-case'
							aria-label='LOG IN'
							disabled={!isFormValid}
							value='legacy'
						>
							Add Brand
						</Button>
					</DialogActions>
				) : (
					<DialogActions className='justify-between pl-16'>
						<Button
							type='submit'
							variant='contained'
							color='primary'
							className='w-full mx-auto mt-16 normal-case'
							aria-label='LOG IN'
							disabled={!isFormValid}
							value='legacy'
						>
							Save Brand
						</Button>
						<IconButton onClick={handleRemove}>
							<Icon>delete</Icon>
						</IconButton>
					</DialogActions>
				)}
			</Formsy>
		</Dialog>
	);
};

export default BrandsDialog;
