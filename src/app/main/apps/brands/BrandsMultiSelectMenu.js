import React, { useState } from 'react';
import { Icon, IconButton, ListItemIcon, ListItemText, Menu, MenuItem, MenuList } from '@material-ui/core';
import * as Actions from './store/actions';
import { useDispatch, useSelector } from 'react-redux';

const BrandsMultiSelectMenu = (props) => {
	const dispatch = useDispatch();
	const selectedBrandIds = useSelector(({ brands: { brands } }) => brands.selectedBrandIds);

	const [ anchorEl, setAnchorEl ] = useState(null);

	function openSelectedBrandMenu(event) {
		setAnchorEl(event.currentTarget);
	}

	function closeSelectedBrandsMenu() {
		setAnchorEl(null);
	}

	return (
		<React.Fragment>
			<IconButton
				className='p-0'
				aria-owns={anchorEl ? 'selectedBrandsMenu' : null}
				aria-haspopup='true'
				onClick={openSelectedBrandMenu}
			>
				<Icon>more_horiz</Icon>
			</IconButton>
			<Menu
				id='selectedBrandsMenu'
				anchorEl={anchorEl}
				open={Boolean(anchorEl)}
				onClose={closeSelectedBrandsMenu}
			>
				<MenuList>
					<MenuItem
						onClick={() => {
							dispatch(Actions.removeBrands(selectedBrandIds));
							closeSelectedBrandsMenu();
						}}
					>
						<ListItemIcon className='min-w-40'>
							<Icon>delete</Icon>
						</ListItemIcon>
						<ListItemText primary='Remove' />
					</MenuItem>
				</MenuList>
			</Menu>
		</React.Fragment>
	);
};

export default BrandsMultiSelectMenu;
