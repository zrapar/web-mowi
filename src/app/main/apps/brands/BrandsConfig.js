import React from 'react';
import { Redirect } from 'react-router-dom';

export const BrandsConfig = {
	settings : {
		layout : {
			config : {}
		}
	},
	routes   : [
		{
			path      : '/apps/brands/:id',
			component : React.lazy(() => import('./Brands'))
		},
		{
			path      : '/apps/brands',
			component : () => <Redirect to='/apps/brands/all' />
		}
	]
};
