import axios from 'axios';
import { logoutUser } from 'app/auth/store/actions';
import { showMessage } from 'app/store/actions';

export const GET_BRANDS = '@@Brands / GET BRANDS';
export const SET_SEARCH_TEXT = '@@Brands / SET SEARCH TEXT';
export const TOGGLE_IN_SELECTED_BRANDS = '@@Brands / TOGGLE IN SELECTED BRANDS';
export const SELECT_ALL_BRANDS = '@@Brands / SELECT ALL BRANDS';
export const DESELECT_ALL_BRANDS = '@@Brands / DESELECT ALL BRANDS';
export const OPEN_NEW_BRAND_DIALOG = '@@Brands / OPEN NEW BRAND DIALOG';
export const CLOSE_NEW_BRAND_DIALOG = '@@Brands / CLOSE NEW BRAND DIALOG';
export const OPEN_EDIT_BRAND_DIALOG = '@@Brands / OPEN EDIT BRAND DIALOG';
export const CLOSE_EDIT_BRAND_DIALOG = '@@Brands / CLOSE EDIT BRAND DIALOG';
export const ADD_BRAND = '@@Brands / ADD BRAND';
export const UPDATE_BRAND = '@@Brands / UPDATE BRAND';
export const REMOVE_BRAND = '@@Brands / REMOVE BRAND';
export const REMOVE_BRANDS = '@@Brands / REMOVE BRANDS';

export const getBrands = (routeParams) => async (dispatch) => {
	try {
		const res = await axios.get('/brands', {
			params : routeParams
		});
		const parseData = JSON.parse(res.data);
		const { success, data } = parseData;
		if (success) {
			dispatch({
				type        : GET_BRANDS,
				payload     : data,
				routeParams
			});
		}
	} catch (err) {
		dispatch(logoutUser());
	}
};

export const setSearchText = (event) => ({
	type       : SET_SEARCH_TEXT,
	searchText : event.target.value
});

export const toggleInSelectedBrands = (brandId) => ({
	type    : TOGGLE_IN_SELECTED_BRANDS,
	brandId
});

export const selectAllBrands = () => ({
	type : SELECT_ALL_BRANDS
});

export const deSelectAllBrands = () => ({
	type : DESELECT_ALL_BRANDS
});

export const openNewBrandsDialog = () => ({
	type : OPEN_NEW_BRAND_DIALOG
});

export const closeNewBrandsDialog = () => ({
	type : CLOSE_NEW_BRAND_DIALOG
});

export const openEditBrandsDialog = (data) => ({
	type : OPEN_EDIT_BRAND_DIALOG,
	data
});

export const closeEditBrandsDialog = () => ({
	type : CLOSE_EDIT_BRAND_DIALOG
});

export const addBrand = (newBrand) => async (dispatch) => {
	try {
		const res = await axios.post('/brands', newBrand, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : ADD_BRAND
				})
			]).then(() => dispatch(getBrands()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const updateBrand = (brand, uuid) => async (dispatch) => {
	try {
		const res = await axios.post(`/brands/${uuid}`, brand, {
			headers : {
				'Content-Type' : 'multipart/form-data'
			}
		});

		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : UPDATE_BRAND
				})
			]).then(() => dispatch(getBrands()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeBrand = (brandId) => async (dispatch) => {
	try {
		const res = await axios.delete(`/brands/${brandId}`);
		const parseData = JSON.parse(res.data);
		const { success, message } = parseData;

		if (success) {
			Promise.all([
				dispatch({
					type : REMOVE_BRAND
				})
			]).then(() => dispatch(getBrands()));
			dispatch(
				showMessage({
					message,
					variant : 'success'
				})
			);
		}
	} catch (err) {
		console.log(err);
	}
};

export const removeBrands = (brandIds) => (dispatch) => {
	let arrayPromise = [];
	brandIds.forEach((uuid) => {
		const req = axios.delete(`/brands/${uuid}`);
		arrayPromise.push(req);
	});

	arrayPromise.push(
		dispatch({
			type : REMOVE_BRANDS
		})
	);

	Promise.all(arrayPromise).then(() => dispatch(getBrands()));
	dispatch(
		showMessage({
			message : 'Successfully deleted',
			variant : 'success'
		})
	);
};
