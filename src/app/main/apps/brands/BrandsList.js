import React, { useEffect, useState } from 'react';
import { Avatar, Checkbox, Icon, IconButton, Typography } from '@material-ui/core';
import { FuseUtils, FuseAnimate } from '@fuse';
import { useDispatch, useSelector } from 'react-redux';
import ReactTable from 'react-table';
import * as Actions from './store/actions';
import BrandsMultiSelectMenu from './BrandsMultiSelectMenu';

const BrandsList = (props) => {
	const dispatch = useDispatch();
	const brands = useSelector(({ brands: { brands } }) => brands.entities);
	const selectedBrandIds = useSelector(({ brands: { brands } }) => brands.selectedBrandIds);
	const searchText = useSelector(({ brands: { brands } }) => brands.searchText);

	const [ filteredData, setFilteredData ] = useState(null);

	useEffect(
		() => {
			function getFilteredArray(entities, searchText) {
				const arr = Object.keys(entities).map((id) => entities[id]);
				if (searchText.length === 0) {
					return arr;
				}
				return FuseUtils.filterArrayByString(arr, searchText);
			}

			if (brands) {
				setFilteredData(getFilteredArray(brands, searchText));
			}
		},
		[ brands, searchText ]
	);

	if (!filteredData) {
		return null;
	}

	if (filteredData.length === 0) {
		return (
			<div className='flex flex-1 items-center justify-center h-full'>
				<Typography color='textSecondary' variant='h5'>
					There are no brands!
				</Typography>
			</div>
		);
	}

	return (
		<FuseAnimate animation='transition.slideUpIn' delay={300}>
			<ReactTable
				className='-striped -highlight h-full sm:rounded-16 overflow-hidden'
				getTrProps={(state, rowInfo, column) => {
					return {
						className : 'cursor-pointer',
						onClick   : (e, handleOriginal) => {
							if (rowInfo) {
								dispatch(Actions.openEditBrandsDialog(rowInfo.original));
							}
						}
					};
				}}
				data={filteredData}
				columns={[
					{
						Header    : () => (
							<Checkbox
								onClick={(event) => {
									event.stopPropagation();
								}}
								onChange={(event) => {
									event.target.checked
										? dispatch(Actions.selectAllBrands())
										: dispatch(Actions.deSelectAllBrands());
								}}
								checked={
									selectedBrandIds.length === Object.keys(brands).length &&
									selectedBrandIds.length > 0
								}
								indeterminate={
									selectedBrandIds.length !== Object.keys(brands).length &&
									selectedBrandIds.length > 0
								}
							/>
						),
						accessor  : '',
						Cell      : (row) => {
							return (
								<Checkbox
									onClick={(event) => {
										event.stopPropagation();
									}}
									checked={selectedBrandIds.includes(row.value.id)}
									onChange={() => dispatch(Actions.toggleInSelectedBrands(row.value.id))}
								/>
							);
						},
						className : 'justify-center',
						sortable  : false,
						width     : 64
					},
					{
						Header    : '',
						accessor  : 'image',
						Cell      : (row) => <Avatar className='mr-8' alt={row.original.name} src={row.value} />,
						className : 'justify-center',
						width     : 64,
						sortable  : false
					},
					{
						Header     : 'Name',
						accessor   : 'name',
						filterable : true,
						className  : 'font-bold'
					},
					{
						Header     : 'Color in App',
						accessor   : 'color',
						Cell       : (row) => (
							<div className='flex items-center'>
								<span>{row.value}</span>
								<div
									style={{
										backgroundColor  : row.value,
										width            : 20,
										height           : 20,
										display          : 'inline-block',
										marginLeft       : 20,
										borderBlockColor : '#212121',
										borderWidth      : 1
									}}
								/>
							</div>
						),
						filterable : true,
						className  : 'font-bold'
					},
					{
						Header    : 'Background image in App',
						accessor  : 'background',
						Cell      : (row) => <Avatar className='mr-8' alt={row.original.name} src={row.value} />,
						className : 'justify-center',
						width     : 64,
						sortable  : false
					},
					{
						Header : () => selectedBrandIds.length > 0 && <BrandsMultiSelectMenu />,
						width  : 128,
						Cell   : (row) => (
							<div className='flex items-center'>
								<IconButton
									onClick={(ev) => {
										ev.stopPropagation();
										dispatch(Actions.removeBrand(row.original.uuid));
									}}
								>
									<Icon>delete</Icon>
								</IconButton>
							</div>
						)
					}
				]}
				defaultPageSize={10}
				noDataText='No brands found'
			/>
		</FuseAnimate>
	);
};

export default BrandsList;
