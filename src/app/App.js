import '@fake-db';
import React from 'react';
import { FuseAuthorization, FuseLayout, FuseTheme } from '@fuse';
import Provider from 'react-redux/es/components/Provider';
import { Router } from 'react-router-dom';
import jssExtend from 'jss-extend';
import history from '@history';
import { Auth } from './auth';
import { store, persistor } from './store';
import { PersistGate } from 'redux-persist/integration/react';
import AppContext from './AppContext';
import routes from './fuse-configs/routesConfig';
import { create } from 'jss';
import { StylesProvider, jssPreset, createGenerateClassName } from '@material-ui/styles';
import { FuseSplashScreen } from '@fuse';

const jss = create({
	...jssPreset(),
	plugins        : [ ...jssPreset().plugins, jssExtend() ],
	insertionPoint : document.getElementById('jss-insertion-point')
});

const generateClassName = createGenerateClassName();

const App = () => {
	return (
		<AppContext.Provider
			value={{
				routes
			}}
		>
			<StylesProvider jss={jss} generateClassName={generateClassName}>
				<Provider store={store}>
					<PersistGate loading={<FuseSplashScreen />} persistor={persistor}>
						<Auth>
							<Router history={history}>
								<FuseAuthorization>
									<FuseTheme>
										<FuseLayout />
									</FuseTheme>
								</FuseAuthorization>
							</Router>
						</Auth>
					</PersistGate>
				</Provider>
			</StylesProvider>
		</AppContext.Provider>
	);
};

export default App;
