import * as reduxModule from 'redux';
import { applyMiddleware, compose, createStore } from 'redux';
import createReducer from './reducers';
import thunk from 'redux-thunk';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage'; // defaults to localStorage for web

const persistConfig = {
	key             : 'mowi',
	storage,
	stateReconciler : autoMergeLevel2,
	blacklist       : [ 'fuse' ]
};

/*
Fix for Firefox redux dev tools extension
https://github.com/zalmoxisus/redux-devtools-instrument/pull/19#issuecomment-400637274
 */
reduxModule.__DO_NOT_USE__ActionTypes.REPLACE = '@@redux/INIT';

const composeEnhancers =
	process.env.NODE_ENV !== 'production' && typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
		? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__(
				{
					// Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
				}
			)
		: compose;

const enhancer = composeEnhancers(applyMiddleware(thunk));
const persistedReducer = persistReducer(persistConfig, createReducer());

const storeVar = createStore(persistedReducer, enhancer);

storeVar.asyncReducers = {};

export const injectReducer = (key, reducer) => {
	if (storeVar.asyncReducers[key]) {
		return;
	}
	storeVar.asyncReducers[key] = reducer;
	storeVar.replaceReducer(createReducer(storeVar.asyncReducers));
	return storeVar;
};

const persistorVar = persistStore(storeVar);

export const store = storeVar;
export const persistor = persistorVar;
